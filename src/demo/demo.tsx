import React from "react"


// Interfaces

interface IMyObject {
    value: string
}

const MyObject: IMyObject = {
    value: "Test"
}

// Generics

interface IMyGenericObject<T> {
    value: T
}

const MyStringObject: IMyGenericObject<string> = {
    value: "Test"
}

const MyNumberObject: IMyGenericObject<number> = {
    value: 1
}

// Components

// Function component
const HelloFunctionComponent = ({ name }: { name: string }) => {
    return <p>Hello {name}</p>
}

<HelloFunctionComponent name={"John Doe"} />

// Class component

class HelloClassComponent extends React.Component<{ name: string }, {}> {
    render = () => {
        return <p>Hello {this.props.name}</p>
    }
}

<HelloClassComponent name={"John Doe"} />

// Nested components

interface IPrinter {
    value: string
}

class PrinterComponent extends React.Component<IPrinter, {}> {
    render = () => {
        const { value } = this.props
        return (
            <div className="Printer__component">
                <HelloClassComponent name={value} />
            </div>
        )
    }
}

<PrinterComponent value="John Doe" />