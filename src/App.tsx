import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

const LogoComponent = () => <img src={logo} className="App-logo" alt="logo" />

const TextComponent = () => {
  return (
    <p>
      Edit <code>src/App.tsx</code> and save to reload.
  </p>
  )
}

const LinkComponent = () => {
  return (
    <a
      className="App-link"
      href="https://reactjs.org"
      target="_blank"
      rel="noopener noreferrer"
    >
      Learn React
  </a>
  )
}

class App extends Component {
  render = () => {
    return (
      <div className="App">
        <header className="App-header">
          <LogoComponent />
          <TextComponent />
          <LinkComponent />
        </header>
      </div>
    )
  }
}

export default App;
